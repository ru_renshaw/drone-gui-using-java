
package gUIDroneSimulation;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import gUIDroneSimulation.MyCanvas;

import gUIDroneSimulation.DroneArena;

public class DroneViewer extends Application {
	private int canvasSize = 512;				// Constant for canvas size
	private MyCanvas mc; 						// Canvas in which system drawn
    private DroneArena theArena;				// Object for solar system
    private VBox rtPane;						// Vertical box for displaying status of planets
    private boolean animationOn = false;		// Whether or not the drones are currently moving

	/**
	 * Show size of arena and where drones are, in pane on right
	 */
	public void drawStatus() {
		rtPane.getChildren().clear();				// clear rtpane
		// Now create label
		Label l = new Label(theArena.toString());
		rtPane.getChildren().add(l);				// add label to pane	
	}
	/**
	 * Draw the drones in the arena onto the canvas
	 */
	public void displayArena() {
		mc.clearCanvas(); 		// Clears the canvas
		theArena.drawArena(mc);	// Draws the arena onto the canvas
		drawStatus();			// Adds the status to the right hand side of the arena
	}
	
	 /**
	  * Function to show a message, 
	  * @param TStr		title of message block
	  * @param CStr		content of message
	  */
	private void showMessage(String TStr, String CStr) {
		    Alert alert = new Alert(AlertType.INFORMATION);
		    alert.setTitle(TStr);
		    alert.setHeaderText(null);
		    alert.setContentText(CStr);

		    alert.showAndWait();
	}
   /**
	 * Shows information about this GUI
	 */
	 private void showAbout() {
		 showMessage("About", "R.Renshaw's Drone GUI");
	 }
	 
	 /**
	  * Shows information about this GUI
	  */
	 private void showHelp() {
		 showMessage("Help", "At the bottom left there is the start and pause buttons."
		 		+ "Use these to continue and halt the movement of drones respectively."
		 		+ "Next to these at the bottom are buttons to add each type of drone."
		 		+ "The last button at the bottom labeled 'Clear' removes all the drones in the arena."
		 		+ "The pannel to the right shows the size of the arena and the location and heading of each drone in it."
		 		+ "At the top there are the 'File', 'Help', and 'Arena' drop down menus."
		 		+ "The File menu has options to exit the GUI, save the current configurement, or load a saved configurement");
	 }
	 
	 /**
	  * Creates a new arena of the size given and changes the size of the canvas showing the arena
	  * @param newSize		The size for the arena and canvas to be changed to
	  */
	 private void changeArenaSize(int newSize){
		canvasSize = newSize;
		theArena = new DroneArena(canvasSize, canvasSize);	// Creates a new arena of the new size
		mc.changeCanvasSize(canvasSize, canvasSize);		// Changes the size of the canvas representing the arena
		displayArena();										// Displays the arena
	 }
	 
	 /**
	  * Sets up the menu (the bar at the top)
	  * @param primaryStage	the stage the menu is to be shown on
	  * @return menuBar		the menu to be shown
	  */
	MenuBar setMenu(Stage primaryStage) {
		MenuBar menuBar = new MenuBar();		// Create menu

		Menu mHelp = new Menu("Help");			// Have entry for help
				// Then add sub menus for About and Help
				// Add the item and then the action to perform
		MenuItem mAbout = new MenuItem("About");
		mAbout.setOnAction(new EventHandler<ActionEvent>() {
           @Override
           public void handle(ActionEvent actionEvent) {
           	showAbout();				// Show the about message
           }	
		});
		MenuItem mAdvice = new MenuItem("Help");
		mAdvice.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent actionEvent) {
				showHelp();	// Brings up some information to help the user know how to use the GUI
			}
		});
		mHelp.getItems().addAll(mAdvice, mAbout); 	// Add submenus to Help
		
				// Now add File menu, which here only has Exit
		Menu mFile = new Menu("File");
		MenuItem mExit = new MenuItem("Exit");
		mExit.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {
		        System.exit(0);						// Quit program
		    }
		});
    	// Option to save the arena
		MenuItem save = new MenuItem("Save");
		// Handler
    	save.setOnAction(new EventHandler<ActionEvent>() {
    		public void handle(ActionEvent event) {
    			// Save the current arena and drones in it in the file called theFile.dat
    			theArena.save("theFile");
    		}
    	});
    	// Option to save the arena
    	MenuItem load = new MenuItem("Load");
    	// Handler
    	load.setOnAction(new EventHandler<ActionEvent>() {
    		public void handle(ActionEvent event) {
    			theArena.load("theFile");	// Load the file called theFile.dat
    			// Changes the size of the canvas the arena is on
    			canvasSize = (int)theArena.getLength();
    			mc.changeCanvasSize(canvasSize, canvasSize);
    			displayArena();
    	    }
    	 });
		mFile.getItems().addAll(mExit, save, load);
		// Menu item for creating a new arena with options for size
		Menu mNewArena = new Menu("New Arena");	
		// Adding sub menus for About and Help
		// By adding the item and the action to perform when selected
		MenuItem m128 = new MenuItem("128 x 128");	// Smallest size
		m128.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent actionEvent) {
				changeArenaSize(128);	// Changes arena size to 128 by 128
			}	
		});
		MenuItem m256 = new MenuItem("256 x 256");
		m256.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent actionEvent) {
				changeArenaSize(256);	// Changes arena size to 256 by 256
			}	
		});
		MenuItem m384 = new MenuItem("384 x 384");
		m384.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent actionEvent) {
				changeArenaSize(384);	// Changes arena size to 384 by 384
			}	
		});
		MenuItem m512 = new MenuItem("512 x 512"); // Largest size, is also the default size
		m512.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent actionEvent) {
				changeArenaSize(512);	// Changes arena size to 512 by 512
			}	
		});
		mNewArena.getItems().addAll(m128, m256, m384, m512); 	// Add options of size to the New Arena option
		menuBar.getMenus().addAll(mFile, mHelp, mNewArena);		// Add File, Help, and Arena to the menu bar
		
		return menuBar;					// Return the menu, so can be added
	}

	/**
	 * Sets up the buttons and returns them so they can be added to borderpane
	 * @return HBox with all the bottom buttons in it 
	 */
    private HBox setButtons() {
    	// Button to start/continue the animation and movement of the drones
    	Button btnAnimOn = new Button("Start");
		// Handler
    	btnAnimOn.setOnAction(new EventHandler<ActionEvent>() {
    		@Override
    		public void handle(ActionEvent event) {
    			animationOn = true;
    		}
    	});
    	
    	// Button to pause the animation and movement of the drones
    	Button btnAnimOff = new Button("Pause");
		// Handler
    	btnAnimOff.setOnAction(new EventHandler<ActionEvent>() {
    		@Override
    		public void handle(ActionEvent event) {
    			animationOn = false;
    		}
    	});
    	
    	// Button to add a random drone to the arena and GUI
    	Button addDrone = new Button("Add Random Drone");
		// Handler
    	addDrone.setOnAction(new EventHandler<ActionEvent>() {
    		@Override
    		public void handle(ActionEvent event) {
    			theArena.addRandomDrone();
    			displayArena();
    		}
    	});
    	// Button to add an avoiding drone to the arena and GUI
    	Button addAvoidingDrone = new Button("Add Avoiding Drone");
		// Handler
    	addAvoidingDrone.setOnAction(new EventHandler<ActionEvent>() {
    		@Override
    		public void handle(ActionEvent event) {
    			theArena.addAvoidingDrone();
    			displayArena();
    		}
    	});
    	
    	// Button to add a grouping drone to the arena and GUI
    	Button addGroupingDrone = new Button("Add Grouping Drone");
    	// Handler
    	addGroupingDrone.setOnAction(new EventHandler<ActionEvent>() {
    		@Override
    		public void handle(ActionEvent event) {
    			theArena.addGroupingDrone();
    			displayArena();
    		}
    	});
    	
    	// Button to add a running drone to the arena and GUI
    	Button addRunningDrone = new Button("Add Running Drone");
    	// Handler
    	addRunningDrone.setOnAction(new EventHandler<ActionEvent>() {
    		@Override
    		public void handle(ActionEvent event) {
    			theArena.addRunningDrone();
    			displayArena();
    		}
    	});
    	
    	// Button to add an obstacle to the arena and GUI
    	Button addObstacle = new Button("Add Obstacle");
		// Handler
    	addObstacle.setOnAction(new EventHandler<ActionEvent>() {
    		@Override
    		public void handle(ActionEvent event) {
    			theArena.addObstacle();
    			displayArena();
    		}
    	});

    	// Button to clear all drones from the arena and GUI
    	Button clearArena = new Button("Clear");
		// Handler
    	clearArena.setOnAction(new EventHandler<ActionEvent>() {
    		@Override
    		public void handle(ActionEvent event) {
    			theArena = new DroneArena(canvasSize, canvasSize);
    			displayArena();
    		}
    	});
    	
    	return new HBox(btnAnimOn, btnAnimOff, addDrone, addAvoidingDrone, addGroupingDrone, addRunningDrone, addObstacle, clearArena);
    }

	/**
	 * This is the main function. It sets up canvas, menu, and buttons
	 * @param stagePrimary		The main stage used in the GUI
	 */
	@Override
	public void start(Stage stagePrimary) throws Exception {
		stagePrimary.setTitle("27010038 Drone Arena Viewer");

	    BorderPane bp = new BorderPane();			// Create border pane

	    bp.setTop(setMenu(stagePrimary));			// Create menu at top of GUI

	    Group root = new Group();					// Create group to put canvas in
	    Canvas canvas = new Canvas( canvasSize, canvasSize );
	    											// and canvas to draw in
	    root.getChildren().add( canvas );			// and add canvas to group
		// Create MyCanvas passing context on canvas onto which images put
	    mc = new MyCanvas(canvas.getGraphicsContext2D(), canvasSize, canvasSize);
	    
	    theArena = new DroneArena(canvasSize, canvasSize);	// Create an arena for the drones
	    
	    bp.setCenter(root);							// Put group in centre pane

	    rtPane = new VBox();						// Set vBox for listing data
	    bp.setRight(rtPane);						// Put in right pane

	    Scene scene = new Scene(bp, canvasSize*1.6, canvasSize*1.2); // Create scene bigger than the canvas

	    new AnimationTimer(){		
	    	// Create handler for animating the drones
	    	public void handle(long currentNanoTime) {
	    		// Calls the necessary functions to run the simulation
	    		if (animationOn) {	// If the animation is on (if the user has pressed Start)
	    			theArena.moveAllDrones();    // Moves all the drones and each has a 1/200 chance of changing direction
	    			displayArena();					// now clear canvas and draw system
	    		}	
	    	}
	    }.start();						// Start the animation

	    bp.setBottom(setButtons());		// Adds the buttons to the bottom of the GUI

		stagePrimary.setScene(scene);
		stagePrimary.show();
	}
	
	/**
	 * This triggers the GUI to start by calling launch
	 * @param args		The arguments given upon calling this GUI
	 */
	public static void main(String[] args) {
		Application.launch(args);			// Start the GUI
	}

}
