# Drone GUI using Java

A simple drone simulation with a GUI. Has options for adding several types of drones and for saving and loading. Created using Java and JavaFX.

At the top of the GUI there is a menu. Pressing on File will make a drop down menu appear with the options Exit, Save, and Load. Exit will cause the GUI to close without automatically saving the simulation. The save button next to/under it stores the current arena and set up of the drones into a file. The load button under the save button can be used to load in an arena and drones that are currently stored in the file.

The Help button will bring up a drop down menu with options Help and About. Selecting Help causes some information on how to use the GUI to be shown and about shows what this GUI is.

The New Arena button brings makes a drop down menu with options for the size of the arena. Pressing on a size will change the arena to be that size.

The middle section shows the arena and the drones within it.

The panel to the right of the GUI contains information about the arena and the drones with in it. At the top it displays the size of the arena, then underneath lists the drones, including their drone number, location, and which direction they are facing in compass terms.

The start and pause buttons are at the bottom left of the GUI. Use the start button to continue the simulation, and the pause button to halt it.

At the bottom of the GUI there is a set of buttons for adding drones to the simulation. Each type of drone has different behaviours. The “Add Random Drone” button will add a drone that will turn randomly as it moves, the “Add Avoiding Drone” button will add a drone that avoids other drones, the “Add Grouping Drone” button will add a drone that moves towards other drones, the “Add Running Drone” button will add a drone that moves towards another drone until it is close to it, and the “Add Obstacle” will add a stationary drone larger than the other drones.

At the bottom right of the GUI is a button labelled Clear, this removes all drones from the arena.