package gUIDroneSimulation;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

/**
 * Class to handle a canvas
 * @author sz010038
 */
public class MyCanvas {
	// Size of the canvas with default values set
	int xCanvasSize = 512;
	int yCanvasSize = 512;
    GraphicsContext gc; 

    /**
     * Constructor sets up relevant Graphics context and size of canvas
     * @param g		the graphics context
     * @param xcs	canvas x dimension size
     * @param ycs	canvas y dimension size
     */
    public MyCanvas(GraphicsContext g, int xcs, int ycs) {
    	gc = g;
    	xCanvasSize = xcs;
    	yCanvasSize = ycs;
    }
    /**
     * Getter for the x size of the canvas
     * @return xCanvasSize
     */
    public int getXCanvasSize() {
    	return xCanvasSize;
    }
    /**
     * Getter for the y size of the canvas
     * @return yCanvasSize
     */
    public int getYCanvasSize() {
    	return yCanvasSize;
    }
    /**
     * Clear the canvas
     */
    public void clearCanvas() {
		gc.clearRect(0,  0,  xCanvasSize,  yCanvasSize);		// Clear canvas
		// Re-draw canvas
		gc.setFill(Color.LIGHTGREY);
		gc.fillRect(0, 0, xCanvasSize, yCanvasSize);
    }
	/**
     * Draws object defined by given image at position and size
     * @param i		image
     * @param x		x position
     * @param y		y position
     * @param sz	size
     */
	public void drawImage (Image i, double x, double y, double sz) {
		// To draw centred at x,y, give top left position and x,y size
		// Sizes/position in range 0.. canvassize 
		gc.drawImage(i, x - sz/2, y - sz/2, sz, sz);
	}

	
	/** 
	 * Function to convert char c to actual colour used
	 * @param c		the character used to represent the colour
	 * @return ans	The colour that corresponds to the character given
	 */
	Color colFromChar (char c){
		Color ans = Color.BLACK;
		switch (c) {
		case 'y' :	ans = Color.YELLOW;
					break;
		case 'w' :	ans = Color.WHITE;
					break;
		case 'r' :	ans = Color.RED;
					break;
		case 'g' :	ans = Color.GREEN;
					break;
		case 'b' :	ans = Color.BLUE;
					break;
		case 'o' :	ans = Color.ORANGE;
					break;
		case 'm' :	ans = Color.GREY;
					break;
		case 'd' :	ans = Color.DARKMAGENTA;
					break;
		case 'p' :	ans = Color.PLUM;
					break;
		}
		return ans;
	}
	
	/**
	 * Set the fill colour to colour c
	 * @param c		the colour
	 */
	public void setFillColour (Color c) {
		gc.setFill(c);
	}
	/**
	 * Show the drone at position x, y, radius r in the colour defined by col
	 * @param x		the x location
	 * @param y		the y location
	 * @param rad	the radius of the drone
	 * @param col	the colour
	 */
	public void showCircle(double x, double y, double rad, char col) {
	 	setFillColour(colFromChar(col));			// Set the fill colour
	 	showCircle(x, y, rad);						// Show the circle
	}

	/**
	 * Show the drone in the current colour at x, y, size rad
	 * @param x		the x location
	 * @param y		the y location
	 * @param rad	the radius of the drone
	 */
	public void showCircle(double x, double y, double rad) {
		gc.fillArc(x-rad, y-rad, rad*2, rad*2, 0, 360, ArcType.ROUND);	// Fill circle
	}

	/**
	 * Changes the size of the canvas to the arguments given.
	 * Then re-draws the canvas
	 * @param x		the new canvas size x dimension
	 * @param y		the new canvas size y dimension
	 */
	public void changeCanvasSize(int x, int y){
		xCanvasSize = x;				// Updates x size
		yCanvasSize = y;				// Updates y size
		gc.clearRect(0,  0,  512,  512);// Clears canvas
		clearCanvas(); 					// Redraws the canvas with the new size
	}
}

