/**
 * 
 */
package gUIDroneSimulation;

/**
 * @author sz010038
 *
 */

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import gUIDroneSimulation.MyCanvas;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import gUIDroneSimulation.Drone;

/**
 * The drone arena has an array list of drones that are inside it.
 * It has bounds of length and width that the drones can move within.
 * It has methods for moving the drones, and saving and loading the arena and the drones within it.
 * @author sz010038
 */
public class DroneArena {
	private double length; 				// Length of arena (x)
	private double width; 				// Width of arena (y)
	private static int numDrones = 0; 	// Number of drones within arena
	protected ArrayList<Drone> drones = new ArrayList<Drone>(); // Array list of the drones
	
	/**
	 * Constructor for DroneArena class
	 * @param lengthArg		length of the arena
	 * @param widthArg		width of the arena
	 */
	public DroneArena(double lengthArg, double widthArg){
		this.length = lengthArg; 	// Sets the length of the arena to the argument length given
		this.width = widthArg;  	// Sets the width of the arena to the argument width given
		Drone.resetDroneCount(); 	// Resets the drone count to 0
	}
	
	/**
	 * Creates a string that contains the information about the arena and the drones within it
	 * @return theString
	 */
	public String toString(){
		String theString = "-----"; //creates string variable
		theString += "Arena size is " + length + " by " + width; // adds info on arena
		theString += "--------------------" + "\n";
		for (Drone aDrone : drones) { // for each drone
		    theString += aDrone.toString(); // adds info on drone
		}
		return theString;
	}
	
	/**
	 * Saves the information about the arena and the drones into a dat file
	 * @param fileName	the name of the file to save to
	 */
	public void save(String fileName){
		try{
			File theFile = new File(fileName+".dat"); // Specifies file
			if ( theFile.exists( ) ) { 		// If the specified file already exists
				theFile.delete(); 			// Removes any existing file with same name and directory, for all intents and purposes overwriting it
			}
			theFile.createNewFile(); // Creates file
			
			// Save length, width and array of drones:
			FileOutputStream stream = new FileOutputStream(theFile); // file output stream
			String fileData = length + " " + width + " " + numDrones; // info on arena
			
			for (Drone aDrone : drones) { // For each drone save x, y, Vx, Vy, size, colour, and maxSpeed
				fileData += "\n" + aDrone.getInfoToSave();
			}
			
			// writes the data to the file and closes the output stream:
			stream.write(fileData.getBytes());
			stream.flush();
			stream.close();

			System.out.print("Saved! \n"); // Informs the user that the arena has been successfully saved
			
	 	} catch(IOException e) { // in case file name given is not valid as a file name such as containing invalid symbols
			System.out.print("failed to find or create file. \n" + e);
	 	}
	}
	
	
	/**
	 * Saves the DroneArena, and the drones in it, in a file, the name of which is inputed by the user 
	 * @param input		scanner used to get an input
	 */
	 public void getFileNameAndSave(Scanner input){ // gets Scanner so can get input
		System.out.print("File name > ");	// asks for file name
		String fileName = input.next();		// gets file name
		System.out.print("Saving... \n");	// informs the user it's starting to save the arena
		save(fileName); // calls save to save the file
	 }
	 
	 /**
	  * Outputs a list of all of the files in the folder Arena (which should only be files containing drone arenas)
	  */
	 public void avalibleFiles(){
		 File directory = new File("N:/My Documents/Java/FirstGo/src/gUIDroneSimulation/Arenas"); // Specifies directory where arenas should be 
		 String filename[] = directory.list(); // creates a list of all the files in that directory
		 for (int i = 0; i < filename.length; i++) { // for each item (file) in that list
			if (filename[i].endsWith(".dat")){ // if correct type of file
				System.out.println(filename[i]); // displays the file name	
			}
		 }
	 }
	 
	 /**
	  * Loads a DroneArena from a file
	  * @param fileName	the name of the file to load from
	  */
	 public void load(String fileName){ // gets Scanner so can get input
		System.out.print("Loading "+fileName+".dat... \n"); // telling the user it's starting to load the file
		//File theFile = new File("N:/My Documents/Java/FirstGo/src/gUIDroneSimulation/Arenas/"+fileName+".dat"); //finds file
		File theFile = new File(fileName+".dat"); //finds file
		if ( theFile.exists( ) ) { // if that file exists
			// Attempts to open the file
			// https://stackoverflow.com/questions/34258810/how-do-i-read-in-a-line-from-a-dat-file-in-java-that-then-needs-to-be-separated
			String[] lines;
			try {
				lines = Files.readAllLines(theFile.toPath()).toArray(new String[0]);
				//length = Integer.parseInt(line.substring(0, 0));
				int count = 0;
				String[] dataInLine = new String[3];
				this.drones.removeAll(drones); // empties the array list drones
				
				for(String line: lines) { // for each line
					dataInLine = line.split(" "); // Splits the line so each value is separate
					if (count == 0){ // if data on arena
						// changes size of arena to size specified in the file:
						this.length = Double.parseDouble(dataInLine[0]);
						this.width = Double.parseDouble(dataInLine[1]);
						// changes number of drones to number specified in the file:
						this.numDrones = Integer.parseInt(dataInLine[2]);
					}
					else{ // If not the first line hence is data on a drone
						// Adds drone to the drone arena using data from file:
						// Saved each drone as "\n" + type + x + " " + y + " " + Vx + " " + Vy + " " + size + " " + colour + " " + maxSpeed
						// Depending on the type of drone being added
						if (Double.parseDouble(dataInLine[0]) == 0.0){		// If plain drone
							this.drones.add(new Drone(Double.parseDouble(dataInLine[1]), Double.parseDouble(dataInLine[2]), Double.parseDouble(dataInLine[3]),  
								Double.parseDouble(dataInLine[4]), dataInLine[6].charAt(0), Double.parseDouble(dataInLine[5]), Double.parseDouble(dataInLine[7]))); 
						}
						else if (Double.parseDouble(dataInLine[0]) == 1.1){	// If avoiding drone
							this.drones.add(new AvoidingDrone(Double.parseDouble(dataInLine[1]), Double.parseDouble(dataInLine[2]), Double.parseDouble(dataInLine[3]),  
									Double.parseDouble(dataInLine[4]), dataInLine[6].charAt(0))); 
						}
						else if (Double.parseDouble(dataInLine[0]) == 1.2){	// If grouping drone
							this.drones.add(new GroupingDrone(Double.parseDouble(dataInLine[1]), Double.parseDouble(dataInLine[2]), Double.parseDouble(dataInLine[3]),  
									Double.parseDouble(dataInLine[4]))); 
						}
						else if (Double.parseDouble(dataInLine[0]) == 1.3){	// If running drone
							this.drones.add(new RunningDrone(Double.parseDouble(dataInLine[1]), Double.parseDouble(dataInLine[2]), Double.parseDouble(dataInLine[3]),  
									Double.parseDouble(dataInLine[4]))); 
						}
						else if (Double.parseDouble(dataInLine[0]) == 2.0){	// If random drone
							this.drones.add(new RandomDrone(Double.parseDouble(dataInLine[1]), Double.parseDouble(dataInLine[2]), Double.parseDouble(dataInLine[3]),  
									Double.parseDouble(dataInLine[4]), dataInLine[6].charAt(0))); 
						}
						else{	// If it wasn't any of the known types of drones
							System.out.print("Drone not valid type \n");
						}
					}
				    count ++;
				}
				System.out.print("loaded! \n");
			} catch (IOException e) {	// Catch block in case failed to load arena
				e.printStackTrace();
			}
		}
		else{ // If file wasn't there
			System.out.print("file not found \n"); // Tells the user it couldn't find the file
		}
	 }
	 
	 /**
	  * Gets a user input for the file name then calls load to load it if possible
	  * @param input	 a scanner for text inputs
	  */
	 public void getFileNameANDLoad(Scanner input){ // Gets Scanner so can get input
		System.out.print("Avalible files to load an arena from: \n");
		avalibleFiles(); // Displays a list of avalibleFiles the user can load an arena from
		System.out.print("Name of file to load (don't include .dat) > "); // Asks for file name
		String fileName = input.next();  // Gets file name
		try{
			load(fileName);
		} catch(NumberFormatException e){
			e.printStackTrace();
		}
	 }
	 
	 /**
	  * Checks if the location given in the arguments has a drone at it
	  * @param x			the x coordinate of the location the drone plans to move to
	  * @param y			the y coordinate of the location the drone plans to move to
	  * @param size			the size of the drone
	  * @param ID			the identifier of the drone
	  * @return occupied
	  */
	public boolean loactionOccupied(double x, double y, double size, int ID){
		boolean occupied = false;
		for (Drone aDrone : drones) { 	// For each drone
			if (aDrone.getID() != ID){ 	// If the drone is not the same drone as checking for collisions
				if (aDrone.distanceToDrone(x, y, size) == 0){ // If any part of the drone is at that location
			    	occupied = true;	// Changes occupied to true
			    	break;				// Ends the loop as found the location is occupied
			    }
			}
		}
		return occupied;
	}
	
	/**
	 * If there is a drone at the location it will return that drone and otherwise return null
	 * @param x		the x coordinate of the location
	 * @param y		the y coordinate of the location
	 * @return aDrone or null
	 */
	public Drone getDroneAt(double x, double y) { // Returns either Drone at x,y or null
		if (loactionOccupied(x, y, 0, -1)){ // Calls function to check if specified location has a drone at it
			for (Drone aDrone : drones){ // For each drone
				// If at the drone is at the location
				if ((aDrone.x-aDrone.size <= x)&&(aDrone.x+aDrone.size >= x)&&(aDrone.y-aDrone.size <= y)&&(aDrone.y+aDrone.size >= y))
					return aDrone; // Returns the drone that is at the location
			}
		}
		return null; // If there wasn't a drone there it returns null
	}
	/**
	 * If there is a drone within the area it will return that drone and otherwise return null
	 * @param x			the x location of the middle of the circle being searched
	 * @param y			the y location of the middle of the circle being searched
	 * @param size		the radius of the area around (x,y) being checked
	 * @param ID		the identifier of a drone to be ignored in this search if any
	 * @return aDrone or null
	 */
	public Drone getDroneInArea(double x, double y, double size, int ID) { // returns either Drone at x,y or null
		for (Drone aDrone : drones) { // For each drone
			if (aDrone.getID() != ID){ // If the drone is not the same drone as checking for collisions
				if (aDrone.distanceToDrone(x, y, size) <= 0){ // If any part of the drone is at that location
			    	return aDrone;	// Returns the drone that is in the area being searched
			    }
			}
		}
		return null; // if there wasn't a drone there it returns null
	}
	
	/**
	 * Checks if the arena is full, then if not it randomly generates positions till it finds 
	 * one that is empty then returns that location.
	 * If the arena is full it displays a message saying so on the console and returns null
	 * @param size		the size of the drone
	 * @param ID		the identifier of the drone
	 * @return location or null
	 */

	private double[] findSutibleDroneLoc(double size, int ID){
		// To position randomly:
		Random randomGenerator; // Random object
		randomGenerator = new Random();
		double[] location = {0,0};
		if (numDrones < (length*width)){ // If space for another drone
			do{ // Until a suitable location is found
				// Random x location from radius - (length-radius)
				location[0] = randomGenerator.nextDouble()*(length-Math.pow(size,2)) + size;
				// Random y location from radius - (width-radius)
				location[1] = randomGenerator.nextDouble()*(width-Math.pow(size,2)) + size;
			} while (loactionOccupied(location[0], location[1], size, ID));
			return location; // Returns location for drone
		}
		else{
			System.out.println("Map full"+ (length*width));
			return null;
		}
	}
	
	/**
	 * Generates a random direction
	 * @return random double between 0.5 and -0.5
	 */
	public double RandomValue(){
		Random randomGenerator; 		// Random object
		randomGenerator = new Random();	// Generates a random number
		return randomGenerator.nextDouble() - 0.5;	// Returns the number
	}
	
	/**
	 * Adds a normal drone to a random location within the arena
	 */
	public void addDrone(){
		double[] location = findSutibleDroneLoc(4, -1);
		if (location != null) { // If there is a suitable location
			this.drones.add(new Drone(location[0], location[1], RandomValue(), RandomValue(), 'm', 4, 1)); // Creates drone
			numDrones++; 		// Adds to count of drones
		}
	}
	
	/**
	 * Adds a random drone to a random location within the arena
	 */
	public void addRandomDrone(){
		double[] location = findSutibleDroneLoc(4, -1);
		if (location != null) { // if there is a suitable location
			this.drones.add(new RandomDrone(location[0], location[1], RandomValue(), RandomValue(), 'm')); // creates drone
			numDrones++; //adds to count of drones
		}
	}
	
	/**
	 * Adds a avoiding drone to a random location within the arena
	 */
	public void addAvoidingDrone(){
		double[] location = findSutibleDroneLoc(4, -1);
		if (location != null) { // If there is a suitable location
			this.drones.add(new AvoidingDrone(location[0], location[1], RandomValue(), RandomValue(), 'b')); // creates drone
			numDrones++; 		// Adds to count of drones
		}
	}
	
	public void addGroupingDrone(){
		double[] location = findSutibleDroneLoc(4, -1);
		if (location != null) { 	// If there is a suitable location
			this.drones.add(new GroupingDrone(location[0], location[1], RandomValue(), RandomValue()));  // creates drone
			numDrones++; 			// Adds to count of drones
		}
	}
	
	public void addRunningDrone(){
		double[] location = findSutibleDroneLoc(4, -1);
		if (location != null) { // if there is a suitable location
			this.drones.add(new RunningDrone(location[0], location[1], RandomValue(), RandomValue()));  // creates drone
			numDrones++; //adds to count of drones
		}
	}
	
	/**
	 * Adds an obstacle to a random location within the arena
	 */
	public void addObstacle(){
		double[] location = findSutibleDroneLoc(8, -1);
		if (location != null) { // If there is a suitable location
			this.drones.add(new Drone(location[0], location[1], 0, 0, 'd', 8, 0.0)); // creates drone
			numDrones++; 		// Adds to count of drones
		}
	}
	
	/**
	 * Displays all of the drones within the arena on to the canvas
	 * @param c		The canvas to display the drones on
	 */
	public void showDrones(gUIDroneSimulation.MyCanvas c) {
		// Loop through all the Drones calling the displayDrone method >>
		for (Drone aDrone : drones) { // For each drone
		    aDrone.displayDrone(c);
		}
	}
	
	/**
	 * Returns an interger array with two items which are the size of the arena
	 * @return size
	 */
	public double[] getSize(){
		double[] size = new double[2]; 	// List for x and y
		size[0] = length; 				// adds x to the list
		size[1] = width; 				// adds y to the list
		return size;
	}
	/**
	 * Returns the length of the arena
	 * @return double size
	 */
	public double getLength(){ // Getter for x
		return length;
	}
	/**
	 * Returns the width of the arena
	 * @return double length
	 */
	public double getWidth(){ // Getter for y
		return width;
	}

	/**
	 * Checks if the drone has hit anything and returns the normal of the thing it hit if anything
	 * this is checked by checking if it's within the arena and using locationOccupied
	 * to check there isn't already a drone there
	 * @param facingX	The x location the drone intends to move to
	 * @param facingY	The y location the drone intends to move to
	 * @param size		The size of the drone moving
	 * @param ID		The identifier of the drone moving
	 * @return normal	The normal to what was hit, or -1 if nothing was hit
	 */
	public double hitNormal(double facingX, double facingY, double size, int ID){ // Checks if a drone can move to location
		double normal = -1; // -1 if nothing hit
		 // Checks if within bounds
		// For each wall checks if it's been hit and if so returns the corresponding normal to the wall
		if (facingX-size < 0){ 			// If hit west wall
			normal = Math.PI/2; 		// Normal is east
		}
		else if (facingY-size < 0 ){ 	// If hit south wall
			normal = 0; 				// Normal is north
		}
		else if (facingX+size >= length){// If hit east wall
			normal = Math.PI*1.5; 		// Normal is west
		}
		else if (facingY+size >= width){// If hit north wall
			normal = Math.PI; 			// Normal is south
		}
		else if (loactionOccupied(facingX, facingY, size, ID)){ // Checks if location already taken
			normal = getDroneAt(facingX, facingY).getAngle();
		}
		return normal;
	}
	
	
	/**
	 * Returns boolean value of whether or not the drone can move to the location
	 * this is checked by checking if it's within the arena and using locationOccupied
	 * to check there isn't already a drone there
	 * @param facingX	The x location the drone intends to move to
	 * @param facingY	The y location the drone intends to move to
	 * @param size		The size of the drone moving
	 * @param ID		The identifier of the drone moving
	 * @return free		Boolean of whether the location is available
	 */
	public boolean canMoveHere(double facingX, double facingY, double size, int ID){ // Checks if a drone can move to location
		boolean free = true;
		if (facingX-size <= 0 | facingY-size <= 0 | facingX+size >= length | facingY+size >= width){ // Checks if within bounds
			free = false;	// Not free as the location is outside the arena
		}
		else if (loactionOccupied(facingX,facingY,size, ID)){ // Checks if location already taken
			free = false;	// Not free as there is already a drone there
		}
		return free;
	}
	
	/**
	 * For each drone, an attempt is made to move it.
	 * Checks the speed in case it is going over it's limit, and if so reduces it's speed.
	 * Then carries out adjustments for collision avoidance then attempts to move.
	 */
	public void moveAllDrones(){
		for (Drone aDrone : drones) { // For each drone
			if (aDrone.getSpeed() > aDrone.getMaxSpeed()) { // If the drone is going above it's maximum speed
				aDrone.setSpeedToMax(); 		// Reduces the speed to the maximum
			}
			aDrone.colisionAdvoidence(this);	// Checks for nearby things and attempts to avoid them
		    aDrone.tryToMove(this, 0); 			// Moves the drone if possible
		}
	}

	/* 
	 * Draws the arena in the GUI
	 * @param args MyCanvas mc (the canvas to draw on)
	 */
	public void drawArena(gUIDroneSimulation.MyCanvas mc) {
		for (Drone d : drones){ // for each drone in the arena
			// calls drawImage to get the drone drawn onto the canvas
			drawImage(mc, d.getXLoc()/mc.xCanvasSize, d.getYLoc()/mc.yCanvasSize, d.getSize(), d.getColour());
		}
	}
	
	/**
	 * Draws a circle using the arguments. Is called by drawArena for each drone.
	 * @param mc		Canvas to be drawn on
	 * @param x			x location of centre of circle
	 * @param y			y location of centre of circle
	 * @param sz		Size of circle
	 * @param col		Colour of circle to be drawn
	 */
	public void drawImage (gUIDroneSimulation.MyCanvas mc, double x, double y, double sz, char col) {
		int cs = mc.getXCanvasSize();
		mc.showCircle((x)*cs, (y)*cs, sz, col);		// * canvas size
	}
}
