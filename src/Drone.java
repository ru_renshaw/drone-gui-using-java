/**
 * 
 */
package gUIDroneSimulation;

import gUIDroneSimulation.DroneArena;

/**
 * Drone is a class used for creating, moving and animating drones. It has several child classes that inherit from it.
 * @author sz010038
 *
 */
public class Drone {
	// attributes:
	private static int droneCount = 0; // keeps track of how many drones there are
	protected double x;		// x location of the drone (length)
	protected double y;		// y location of the drone (width)
	protected double size;	//size of the drone
	// Where it is going as a vector;
	protected double Vx;	// x part of vector
	protected double Vy;	// y part of vector
	protected int identifier;	// drones unique identifier
	protected char colour;
	protected double maxSpeed; // The maximum speed of the drone
	protected double colisionAdvoidenceRadius; // The radius the drone can check around it's self for collisions
	
	
	/**
	 *  Constructor with no arguments
	 */
	public Drone() {
		// Calls the other constructor for drone with location (0,0) and velocity (0,-1)
		this(0.0, 0.0, 0.0, -1.0, 'b', 4.0, 1.5);
	}
	
	/**
	 * Constructor with arguments
	 * @param droneX	the x location
	 * @param droneY	the y location
	 * @param xSpeed	the x velocity
	 * @param ySpeed	the y velocity
	 * @param col		the colour
	 * @param radius	the size in terms of it's radius
	 * @param sp		the maximum speed
	 */
	public Drone(double droneX, double droneY, double xSpeed, double ySpeed, char col, double radius, double sp) {
		this.x = droneX;
		this.y = droneY;
		this.identifier = droneCount; // sets the identifier to the current drone count
		droneCount++;	//increments drone count
		this.size = radius;
		this.Vx = xSpeed;
		this.Vy = ySpeed;
		this.colour = col;
		this.maxSpeed = sp;
		this.colisionAdvoidenceRadius = 10;
	}
	
	/**
	 * Displays the drone in the GUI on the canvas representing the arena.
	 * @param c		The canvas that is being drawn on
	 */
	public void displayDrone(gUIDroneSimulation.MyCanvas c) {
		c.showCircle(x, y, 4, colour);
	}
	
	/**
	 * Resets the drone counter to 0
	 *  should only be used when a new group of drones is being used, 
	 *  such as when a new arena is being created
	 */
	public static void resetDroneCount(){
		droneCount = 0;
	}
	
	 /**
	 * Tries to move the drone within the arena
	 * @param a			the DroneArena the drone is within
	 * @param calls		the number of times this has already been called
	 * @return 0
	 */
	public int tryToMove(DroneArena a, int calls){
		double facingX = x+Vx; // Stores x location of location it might move to
		double facingY = y+Vy; // Stores y location of location it might move to
		//double facing = Math.atan(Vy/Vx); // o=aT -> Angle = T^-1(0/a)
		calls ++; // calls stores how many directions it's tried to move in, as a new iteration has started this increments the number of calls
		
		if(a.canMoveHere(facingX, facingY, size, identifier) == true){ // If it can move to where it is facing
			// Changes the location to the one being moved to if it's free
			x = facingX;
			y = facingY;
		}
		else{	// If not able to move to that location, so needs to bounce off
			if (calls < 3){ // If not trapped (gives up trying to move the drone after a few tries)
				// To update using angle it is facing
				double normal = a.hitNormal(facingX, facingY, size, identifier); // Calculates normal to object hit
				// Using which wall or whatever it collided with
				if (normal == 0||normal == Math.PI){ // Top or bottom wall hit
					Vy *= -1; 	// Flips y
				} else if (normal == Math.PI/2 || normal == 3*Math.PI/2) { // Left or right wall hit
					Vx *= -1; 	// Flips x
				} else {		// If hit something else attempts to turn half a circle
					Vx *= -1; 	// Flips x
					Vy *= -1; 	// Flips y
				}
				tryToMove(a, calls);	// Tries again with new values
			}
		}
		return 0;
	}
	
	/**
	 * Checks if there are any drones or walls very nearby so that it can attempt to avoid them
	 * @param a		the DroneArena the drone is within
	 */
	protected void colisionAdvoidence(DroneArena a){
		double xModifier = 0;
		double yModifier = 0;
		if (x-size-colisionAdvoidenceRadius <= 0){ 					// Checks if near West wall
			xModifier = -x;
		}
		else if (y-size-colisionAdvoidenceRadius <= 0){ 			// Checks if near South wall
			yModifier = -y;
		}
		else if (x+size+colisionAdvoidenceRadius >= a.getLength()){ // Checks if near East wall
			xModifier = x-a.getLength();
		}
		else if (y+size+colisionAdvoidenceRadius >= a.getWidth()){ 	// Checks if near North wall
			yModifier = y-a.getWidth();
		}
		if (a.loactionOccupied(x, y, size+colisionAdvoidenceRadius, identifier)){ // If near another drone
			// Gets a/the drone that is within the area
			Drone nearbyDrone = a.getDroneInArea(x, y, size+colisionAdvoidenceRadius, identifier);
			xModifier = -(nearbyDrone.getXLoc()-x);
			yModifier = -(nearbyDrone.getYLoc()-y);
		}
		Vx = Vx + xModifier*0.001;
		Vy = Vy + yModifier*0.001;
	}

	public String toString(){
		String theString = "Drone ";
		theString += identifier + " is at ";
		theString += (int)(x) + ", " + (int)(y);
		if (Vx + Vy == 0){ // If it's not moving
			theString += ", and is stationary";
		}
		else{	// If it is moving
			theString += ", and is going ";
			// To output direction in terms of N, NE, E, SE, S, SW, W, or NW
			if ((Vy*Vy) > ((Vx*Vx))*2){ // If y is much more significant than x
				// Then north or south
				if (Vy < 0) theString += "North";
				else theString += "South";
			}
			else if (Vx*Vx > (Vy*Vy)*2){ // else if x much more significant than y
				// Then east or west
				if (Vx > 0) theString += "East";
				else theString += "West";
			}
			else { // If x and y are similarly significant
				// North or south
				if (Vy < 0) theString += "North";
				else theString += "South";
				// East or West
				if (Vx > 0) theString += "-East";
				else theString += "-West";
			}
		}
		theString += "\n";
		return theString;
	}
	
	/** 
	 * Returns what angle the drone is facing using trigonometry
	 * @return angleFromNorth	the angle the drone is headed in
	 */
	public double getAngle(){
		// Uses o=a*Tan(angle) => Tan^-1(o/a) = angle
		// where o = Vx, and a = Vy
		double angle = Math.atan(Vx/Vy);
		double angleFromNorth = 0;
		// Calculates the angle from north depending on which quadrant the angle it is facing is in
		if (Vy >= 0){ // If y velocity is positive
			if (Vx >= 0)angleFromNorth = angle;				// If x velocity is positive - 1st quadrant
			else		angleFromNorth = (2*Math.PI)-angle; // If x velocity is negative - 2nd quadrant
		}
		else{ // If y velocity is negative
			if (Vx < 0)	angleFromNorth = angle+Math.PI; 	// If x velocity is negative - 3rd quadrant
			else 		angleFromNorth = Math.PI-angle; 	// If x velocity is positive - 4th quadrant
		}
		return angleFromNorth;
	}
	
	/** 
	 * Changes the speed of the drone so that the direction dousn't change
	 */
	public void setSpeedToMax() {
		if (maxSpeed == 0){	// If stationary drone
			Vx = 0;
			Vy = 0;
		} else{
			double angle = getAngle();	// Works out what angle the drone is travelling in
			// Uses the angle and the maximum speed to calculate the new vector
			// using o=hS and a=hC where h=maxSpeed, a=Vx, 0=Vy
			Vx = maxSpeed*Math.cos(angle);
			Vy = maxSpeed*Math.sin(angle);
		}
	}

	/**
	 * Calculates the distance from this drone to the drone given as the argument
	 * Using pythagorises theorem (c^2 = a^2 + b^2)
	 * @param oX		other drones x location
	 * @param oY 		other drones y location
	 * @param oRadius	other drones radius
	 * @return distance between this drone and d
	 */
	protected double distanceToDrone(double oX, double oY, double oRadius){
		// Calculates distance between centres of the drones
		double distance = Math.sqrt(Math.pow((oX-x),2) + Math.pow((oY-y),2));
		distance = distance-oRadius-size;// Subtracts the radius of both drones from the distance
		if (distance < 0) distance = 0; // If distance is negative due to overlap, then set the distance to 0
		return distance;				// Returns the distance
	}
	/**
	 * Calculates the angle from the centre of this drone to a specified location
	 * @param oX		specified x location
	 * @param oY		specified y location
	 * @return angleFromNorth	the angle in radians
	 */
	protected double angleToLoc(double oX, double oY){
		// Uses o=aT(angle) => T^-1(o/a) = angle
		// where o = x, and a = y
		double angle = Math.atan(Math.sqrt(Math.pow((oX-x),2))/Math.sqrt(Math.pow((oY-y),2)));
		double angleFromNorth = 0;	// Angle between this drone and the location from the North
		if (oY >= y){ // If y direction is positive
			if (oX >= x)angleFromNorth = angle;				// If x direction is positive - 1st quadrant
			else		angleFromNorth = (2*Math.PI)-angle; // If x direction is negative - 2nd quadrant
		}
		else{ // If y direction is negative
			if (oX < x)	angleFromNorth = angle+Math.PI; 	// If x direction is negative - 3rd quadrant
			else 		angleFromNorth = Math.PI-angle; 	// If x direction is positive - 4th quadrant
		}
		return angleFromNorth;
	}
	
	/**
	 * Returns information on the drone in a string form to be saved in a text file
	 * @return String of information needed to store the drone
	 */
	public String getInfoToSave(){
		return (getType() + " " + x + " " + y + " " + Vx + " " + Vy + " " + size + " " + colour + " " + maxSpeed); // Returns info on drone
	}
	
	
	/** 
	 * Returns the x location of the drone
	 * @return x	the x location of the drone
	 */
	public double getXLoc() {
		return x;
	}
	/** 
	 * Returns the y location of the drone
	 * @return y 	the y location of the drone
	 */
	public double getYLoc() {
		return y;
	}
	/** Changes the vector the drone is facing
	 * @param x		the new x velocity
	 * @param y		the new y velocity
	 */
	public void setVector(double x, double y) {
		this.Vx = x;
		this.Vy = y;
	}
	/** 
	 * Getter for Vx (the x part of the vector)
	 * @return Vx	the x velocity of the drone
	 */
	public double getVectorX() {
		return Vx;
	}
	/** 
	 * Getter for Vy (the y part of the vector)
	 * @return Vy 	the y velocity of the drone
	 */
	public double getVectorY() {
		return Vy;
	}
	/** 
	 * Returns the size of the drone
	 * @return size 	the size of the drone
	 */
	public double getSize() {
		return size;
	}
	/** 
	 * Returns the colour of the drone
	 * @return colour	the colour of the drone
	 */
	public char getColour() {
		return colour;
	}
	/** Returns the identifier of the drone
	 * @return identifier	the unique identifier of the drone
	 */
	public double getID() {
		return identifier;
	}
	/** 
	 * Returns the current speed of the drone using pythagorous
	 * @return		the speed the current drone is going at
	 */
	public double getSpeed() {
		return Math.sqrt((Vx*Vx)+(Vy*Vy)); // Calculates and returns the speed
	}
	/** 
	 * Returns the maximum speed of the drone
	 * @return maxSpeed		the maximum speed
	 */
	public double getMaxSpeed(){
		return maxSpeed;
	}
	/** 
	 * Returns what type of drone the drone is
	 * @return type of drone this is (0 for base type)
	 */
	public double getType(){
		return 0;
	}
}
