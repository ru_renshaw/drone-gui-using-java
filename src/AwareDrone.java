/**
 * 
 */
package gUIDroneSimulation;
import gUIDroneSimulation.Drone;
import java.util.ArrayList;
/**
 * Abstract class AwareDrone
 * Aware drones have an arrayList of drones that are within a set radius (radiusToCheck) of them.
 * If there are any drones within this radius, the method react is called.
 * This dousn't do anything in this abstract class, but each of the child classes have their own
 *  method for it to allow them to react to nearby drones in various ways.
 * @author sz010038
 */
public class AwareDrone extends Drone {
	// The attributes for AwareDrone
	protected double radiusToCheck; 	// The radius around the drone which will be searched for other drones
	protected ArrayList<Drone> nearbyDrones = new ArrayList<Drone>(); // Array list of the drones within the radius
	
	/**
	 * Constructor with no arguments given
	 */
	public AwareDrone(){
		super(); 			// Runs parent classes constructor
		radiusToCheck = 50; // Sets radius to check to be 50
	}
	
	/**
	 * Constructor with arguments for x, y, and direction
	 * @param droneX	the x location of the drone
	 * @param droneY	the y location of the drone
	 * @param x			the x velocity of the drone
	 * @param y			the y velocity of the drone
	 * @param col		the colour of the drone
	 */
	public AwareDrone(double droneX, double droneY, double x, double y, char col){
		super(droneX, droneY, x, y, col, 4, 1); // Runs parent classes constructor
		radiusToCheck = 50; 					// Sets radius to check to be 5
	}
	/**
	 * Constructor with arguments for x, y, direction, and radius to check
	 * @param droneX	the x location
	 * @param droneY	the y location
	 * @param x			the x velocity
	 * @param y			the y velocity
	 * @param rToCheck	the radius around the drone it is 'aware' of
	 * @param col		the colour of the drone
	 */
	public AwareDrone(double droneX, double droneY, double x, double y, double rToCheck, char col){
		super(droneX, droneY, x, y, col, 4, 1); // Runs parent classes constructor
		radiusToCheck = rToCheck;				// Sets radius to check to the value given in the arguments
	}
	
	/**
	 * Overwrites the parents try to move method for AwareDrones. 
	 * Calls the method checkRadius to check for nearby drones and react if there are any.
	 * Then calls the parents tryToMove method to actually move the drone.
	 * @param a 		the DroneArena this drone is in
	 * @param calls 	the number of times try to move has already been called
	 * @return 	0		what the parent function returns if successfully run
	 */
	public int tryToMove(DroneArena a, int calls){
		if (calls == 0) checkRadius(a); 	// If the first call, then checks for nearby drones
		return super.tryToMove(a, calls);	// Uses parents method to move the drone
	}
	
	/**
	 * Checks an area around the drone to see what drones are in it's checking radius
	 * then if there are any calls react so the drone can react to them
	 * @param arena		- the DroneArena this drone is in
	 */
	protected void checkRadius(DroneArena arena){
		nearbyDrones.clear(); 					// Clears drones currently stored in nearbyDrones
		double distance = 0;					// Holds the distance between this drone and other drones
		for(Drone d : arena.drones){ 			// For each drone in arena
			if (d.getID() != identifier && d.getMaxSpeed() > 0){ 					// If not the same as this drone and not just an obstacle
				distance = distanceToDrone(d.getXLoc(), d.getYLoc(), d.getSize()); 	// Calculates distance between this drone and drone being checked
				if (distance < radiusToCheck){ 										// If the drone is within the radius being checked
					nearbyDrones.add(d); 											// Adds the drone to the list of drones in the radius
				}
			}
		}
		if (!nearbyDrones.isEmpty()){ 	// If there is at least one drone in the radius
			react();					// Calls the function that allows the drone to react to other nearby drones
		}
	}
	
	/**
	 * Does nothing in this abstract class, is place holder to be used by the child classes
	 * in which it will change the direction of the drone based on the nearby drones
	 */
	protected void react(){
		// Left empty in this abstract class each child class will have own react method
	}
	/** Returns what type of drone this drone is
	 * @return type of drone this is (1 for base Aware Drone)
	 */
	public double getType(){
		return 1;
	}
}
