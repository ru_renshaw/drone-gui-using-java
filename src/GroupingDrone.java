/**
 * 
 */
package gUIDroneSimulation;

/**
 * Grouping drones are a type of AwareDrones so have access to an array list of drones that are nearby to them.
 * They use this to move towards other drones and align their direction of movement with that of the other drones
 * @author sz010038
 */
public class GroupingDrone extends AwareDrone {
	/**
	 * Constructor with no arguments given
	 */
	public GroupingDrone(){
		super(); 			// runs parent classes constructor
		this.colour = 'y';	// sets colour to yellow
	}
	
	/**
	 * Constructor with arguments given
	 * @param droneX	the x location of the drone
	 * @param droneY	the y location of the drone
	 * @param x			the x velocity of the drone
	 * @param y			the y velocity of the drone
	 */
	public GroupingDrone(double droneX, double droneY, double x, double y){
		super(droneX, droneY, x, y, 'y'); // runs parent classes constructor
	}
	
	/**
	 * Function to react to nearby drones by moving towards them
	 * Is called by checkRadius if there is a drone in the radius it checks
	 * changes what direction it is facing to move towards other drones
	 * also aligns it's direction with other drones
	 */
	protected void react(){
		double sumXDistance = 0, sumYDistance = 0;	// The sum of the distance to the nearby drones
		double averageX = 0, averageY = 0;			// Average distance from other nearby drones
		double sumVx = 0, sumVy = 0;				// The sum of the nearby drones velocities
		double averageVx = 0, averageVy = 0;		// Average velocity of nearby drones
		double numOfNearbyDrones = nearbyDrones.size();	// Number of nearby drones 
		double numOfAligns = 0; 					// Number of drones close enough to align with
		// Getting and processing the information needed from each drone
		for (Drone closeDrone: nearbyDrones){		// For each nearby drone
			numOfAligns ++;
			// Adds the distance to that drone to the sum
			sumXDistance += (closeDrone.getXLoc()-x);
			sumYDistance += (closeDrone.getYLoc()-y);	
			if (distanceToDrone(closeDrone.getXLoc(), closeDrone.getYLoc(), closeDrone.getSize()) < 30) {// Adds the velocity of that drone to the sum
				sumVx += closeDrone.getVectorX();
				sumVy += closeDrone.getVectorY();
			}
		}
		// Finds the average distance from the sums
		averageX = sumXDistance/numOfNearbyDrones;	
		averageY = sumYDistance/numOfNearbyDrones;
		// Finds average velocity of nearby drones
		averageVx = sumVx/numOfAligns;
		averageVy = sumVy/numOfAligns;
		// Updates velocity to move towards nearby drones and more similar direction
		if (numOfAligns>0){	// If any drones were close enough to align with
			Vx = Vx*0.999 + averageX*0.0002 + averageVx*0.0008;
			Vy = Vy*0.999 + averageY*0.0002 + averageVy*0.0008;
		}
		else{ // If no drones were close enough to align with
			Vx = Vx*0.9994 + averageX*0.0006;
			Vy = Vy*0.9994 + averageY*0.0006;
		}
	}
	
	/** 
	 * Returns what type of drone this drone is
	 * @return type of drone this is (1 for base Aware Drone)
	 */
	public double getType(){
		return 1.2;
	}
}
