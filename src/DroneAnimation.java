package gUIDroneSimulation;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.stage.Stage;

public class DroneAnimation extends Application {
	private int canvasSize = 512;				// size of canvas
    private MyCanvas mc; 						// canvas in which system drawn
    private DroneArena theArena;				// model of system

	/**
	 * main function. Sets up canvas, menu, buttons and timer
	 */
	@Override
	public void start(Stage stagePrimary) throws Exception {
		stagePrimary.setTitle("27010038 Drone Animation");
		
	    Group root = new Group();					// for group of what is shown
	    Scene scene = new Scene( root );			// put it in a scene
	    stagePrimary.setScene( scene );				// apply the scene to the stage
	 
	    Canvas canvas = new Canvas(canvasSize, canvasSize);
	    							// create canvas onto which animation shown
	    root.getChildren().add( canvas );			// add to root and hence stage
	 
	    mc = new MyCanvas(canvas.getGraphicsContext2D(), canvasSize, canvasSize);
	    								// create MyCanvas passing context on canvas onto which images put
	    theArena = new DroneArena(canvasSize, canvasSize);				// Create object for sun, planets, etc
	    
	    new AnimationTimer()			// Create timer
	    	{
	    		public void handle(long currentNanoTime) {
	    			theArena.moveAllDrones(); // moves all the drones
	    			theArena.drawArena(mc);
	    		}
	    	}.start();			// start it
	    
		stagePrimary.show();
	}
	
	public static void main(String[] args) {
		Application.launch(args);			// launch the GUI
	}

}
