package gUIDroneSimulation;

import java.util.Random;

/**
 * These a type of drone which will rotate at random to move in a more natural seeming way around the arena
 * They use a x and y acceleration to reduce sudden changes in direction
 * @author sz010038
 */
public class RandomDrone extends Drone {
	private double Ax;	// Acceleration/deceleration in x direction
	private double Ay;	// Acceleration/deceleration in y direction
	/**
	 * Constructor with no arguments given
	 */
	public RandomDrone(){
		super(); 			// Runs parent classes constructor
		Ax = 0;
		Ay = 0;
	}
	/**
	 * Constructor with arguments for x, y, and direction
	 * @param droneX	x location
	 * @param droneY	y location
	 * @param xSpeed	x velocity
	 * @param ySpeed	y velocity
	 * @param col		colour
	 */
	public RandomDrone(double droneX, double droneY, double xSpeed, double ySpeed, char col){
		super(droneX, droneY, xSpeed, ySpeed, col, 4, 1); 	// Runs parent classes constructor
		Ax = 0;
		Ay = 0;
	}
	
	/**
	 * Uses a random generator to slightly change the acceleration.
	 * This is only done occasionally to make the changes in direction smoother
	 */
	private void changeAcceleration(){
		Random randomGenerator; // Random object
		randomGenerator = new Random();
		// Adds small random values to the acceleration
		Ax += randomGenerator.nextDouble()*0.004 - 0.002; 	// Random value between -0.001 and 0.001
		Ay += randomGenerator.nextDouble()*0.004 - 0.002; 	// Random value between -0.001 and 0.001
		if (Math.sqrt((Ax*Ax)+(Ay*Ay)) >= 0.001){			// If accelerating above a certain amount
			// Reduces acceleration by 5%
			Ax *= 0.95;
			Ay *= 0.95;
		}
	}
	
	/**
	 * Replaces the parents tryToMove function so that it also turns based on the acceleration
	 */
	public int tryToMove(DroneArena a, int calls){
		// At random changes the acceleration
		Random randomGenerator; // Random object
		randomGenerator = new Random();
		if (randomGenerator.nextInt(75) == 0){	// 1 in 75 chance of changing the acceleration each time
			changeAcceleration();				// Changes the acceleration
		}
		// Adds the acceleration to the velocity
		Vx += Ax;
		Vy += Ay;
		return super.tryToMove(a, calls);	// Calls parents function to move the drone
	}
	/** 
	 * Returns what type of drone this drone is
	 * @return type of drone this is (2 for Random Drone)
	 */
	public double getType(){
		return 2;
	}
}
