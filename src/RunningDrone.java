/**
 * 
 */
package gUIDroneSimulation;

import java.util.Random;

/**
 * RunningDrones are a type of AwareDrones so have access to an array list of drones that are nearby to them.
 * They use this to select a drone to move towards until they are very close at which point they may try again
 * or select a new drone to move towards
 * @author sz010038
 */
public class RunningDrone extends AwareDrone {
	private Drone chosenDrone;		// The drone this drone is moving towards
	private boolean chasing;		// Whether the drone has already gone past this drone
	/**
	 * Constructor with no arguments given
	 */
	public RunningDrone(){
		super(); 					// Runs parent classes constructor
		this.radiusToCheck = 70; 		// sets radius to check to be 70
		this.colour = 'p';			// Sets colour to yellow
		this.chosenDrone = this;	// Initialises the chosen drone as it's self
		this.chasing = false;		// Whether it is currently chasing a drone
	}
	
	/**
	 * Constructor with arguments given
	 * @param droneX	the x location of the drone
	 * @param droneY	the y location of the drone
	 * @param x			the x velocity of the drone
	 * @param y			the y velocity of the drone
	 */
	public RunningDrone(double droneX, double droneY, double x, double y){
		super(droneX, droneY, x, y, 'p');	// Runs parent classes constructor
		this.radiusToCheck = 70; 		// sets radius to check to be 70
		this.chosenDrone = this;			// Initialises the chosen drone as it's self
		this.chasing = false;		// Whether it is currently chasing a drone
	}
	
	/**
	 * Reacts to nearby drones by either changing it's velocity to go towards the drone it has chosen,
	 * or if is not chasing a drone, it selects one at random to chase from those nearby
	 */
	protected void react(){
		if (chasing){	// If it's currently chasing a drone
			double distance = distanceToDrone(chosenDrone.getXLoc(), chosenDrone.getYLoc(), chosenDrone.getSize());
			// If the drone is no longer within the drones sensors radius, or is very close
			if (distance >= radiusToCheck || distance <= colisionAdvoidenceRadius){ 
				chasing = false; // Changes chasing to false as too far away or almost reached
			}
		}
		else { // If it's not currently chasing a drone
			// Selects a drone at random from those nearby
			Random randomGenerator; // Random object
			randomGenerator = new Random();
			// 1 in 50 chance of choosing a drone to follow each time
			// This gives it a chance to slow down and find a new group of drones
			if (randomGenerator.nextInt(50) == 0){
				// Sets the chosen drone to be one of the nearby drones selected at random
				// From the arrayList of drones nearby
				chosenDrone = nearbyDrones.get(randomGenerator.nextInt(nearbyDrones.size()));
				// Sets chasing to true as it has selected a drone to chase
				chasing = true;
			}
		}
	}
	
	/**
	 * Replaces the parents tryToMove function so that it also turns
	 */
	public int tryToMove(DroneArena a, int calls){
		if (chasing){	// If it's currently chasing a drone
			// Finds the x and y distance between this drone and it's chosen drone
			double dx = chosenDrone.getXLoc()-x;
			double dy = chosenDrone.getYLoc()-y;
			// Adds a value based on the distance between the drones to the velocity so it heads towards it's chosen drone
			Vx += dx*0.005;
			Vy += dy*0.005;
		}
		else if (this.getSpeed() >= maxSpeed/2){ 	// If not chasing and not going slowly already
			// Reduces the speed by 0.0001
			Vx *= 0.9999;
			Vy *= 0.9999;
		}
		return super.tryToMove(a, calls);	// Calls parents function to move the drone
	}
	/** 
	 * Returns what type of drone this drone is
	 * @return type of drone this is (1 for base Aware Drone)
	 */
	public double getType(){
		return 1.3;
	}
}
