/**
 * 
 */
package gUIDroneSimulation;
import gUIDroneSimulation.AwareDrone;
/**
 * Avoiding drones are a subclass of AwareDrone. They move away from other drones that are within their radiusToCheck
 * It has 3 constructors each for a different number of arguments and replaces it's parents empty react method with
 * its own method that allows it to turn away from nearby drones.
 * @author sz010038
 */
public class AvoidingDrone extends AwareDrone {
	/**
	 * Constructor with no arguments given
	 */
	public AvoidingDrone(){
		super(); 			// Runs parent classes constructor
	}
	/**
	 * Constructor with arguments for x, y, direction, and colour
	 * @param droneX	the x location of the drone
	 * @param droneY	the y location of the drone
	 * @param x			the x velocity of the drone
	 * @param y			the y velocity of the drone
	 * @param col		the colour of the drone
	 */
	public AvoidingDrone(double droneX, double droneY, double x, double y, char col){
		super(droneX, droneY, x, y, col); 	// runs parent classes constructor
	}
	/**
	 * Constructor with arguments for x, y, direction, radius to check, and colour
	 * @param droneX	the x location
	 * @param droneY	the y location
	 * @param x			the x velocity
	 * @param y			the y velocity
	 * @param rToCheck	the radius around the drone it is 'aware' of
	 * @param col		the colour of the drone
	 */
	public AvoidingDrone(double droneX, double droneY, double x, double y, double rToCheck, char col){
		super(droneX, droneY, x, y, rToCheck, col); // runs parent classes constructor
	}
	
	/**
	 * Reacts to nearby drones by moving away from them
	 * Is called by checkRadius if there is a drone in the radius it checks
	 * changes what direction it is facing to move away from nearby drones
	 */
	protected void react(){
		double sumXDistance = 0;	// The sum of the angles to the nearby drones
		double sumYDistance = 0;	// The sum of the angles to the nearby drones
		double averageX = 0;		// Average x distance from other nearby drones
		double averageY = 0;		// Average y distance from other nearby drones
		for (Drone closeDrone: nearbyDrones){	// For each nearby drone
			// Adds the distance to that drone to the sum
			sumXDistance += (closeDrone.getXLoc()-x);
			sumYDistance += (closeDrone.getYLoc()-y);	
		}
		// Finds the average distance from the sums
		averageX = sumXDistance/nearbyDrones.size();	
		averageY = sumYDistance/nearbyDrones.size();
		// Updates velocity to move away from nearby drones
		Vx = Vx*0.9995 - averageX*0.0005;
		Vy = Vy*0.9995 - averageY*0.0005;
	}
	/** Returns what type of drone this drone is
	 * @return type of drone this is (1 for base Aware Drone)
	 */
	public double getType(){
		return 1.1;
	}
}
